import Vue from 'vue';
import Router from 'vue-router';
import Login from '../views/Login';
import Logout from '../views/Logout';
import Example from '../views/Example';

import Register from '../views/Register';

Vue.use(Router);

export default  new  Router({

    routes:[

        {
            path:'/login',
            name:'Login',
            component:Login
        },
        {
            path:'/register',
            name:'Register',
            component:Register
        },
        {
            path:'/logout',
            name:'Logout',
            component:Logout
        },
        {
            path:'/example',
            name:'Example',
            component:Example
        },

    ],
    mode:'history'

});



