import Vue from 'vue'
import App from './App.vue'
import axios from 'axios';
import router from './router';
import Argon from "./plugins/argon-kit";
import store from "./store";

import ApiService from "./common/api.service";

ApiService.init();
Vue.config.productionTip = false;

 window.axios=axios;

Vue.use(Argon);
Vue.config.productionTip = false


new Vue({


    router,
    store,
    render: h => h(App)
}).$mount('#app')