import ApiService from "@/common/api.service";
import PassportService from "@/common/passport.service";

import {
    LOGIN,
    LOGOUT,
    REGISTER,
    CHECK_AUTH,
    UPDATE_USER
} from "./actions.type";
import { SET_AUTH, PURGE_AUTH, SET_ERROR } from "./mutations.type";

const state = {
    token: localStorage.getItem('access_token')||null,
    errors: null,
    user: {},
    isAuthenticated: !!PassportService.getToken()
};

const getters = {
    currentUser(state) {
        return state.user;
    },
    isAuthenticated(state) {
        return state.isAuthenticated;
    },

};

const actions = {
    [LOGIN](context, credentials) {
        return new Promise((resolve, reject)  => {
            ApiService.post("/login", {

                email:credentials.email,
                password:credentials.password,
                remember_me:credentials.remember_me,

            })
                .then(response => {
                    const token=response.data.access_token
                    localStorage.setItem('access_token',token);
                    context.commit(SET_AUTH, token);
                    resolve(response);
                })
                .catch(({ response }) => {
                    context.commit(SET_ERROR, response.data.errors);
                    reject(response);
                });

        });
    },
    [LOGOUT](context) {
        context.commit(PURGE_AUTH);
    },
    [REGISTER](context, credentials) {
        return new Promise((resolve, reject) => {
            ApiService.post("/signup", {
                username: credentials.username,
                email:credentials.email,
                password:credentials.password,
                password_confirmation:credentials.password_confirmation,
                account_type_id:credentials.account_type_id
            })
                .then(response => {
                    const token=response.data.access_token
                    localStorage.setItem('access_token',token);
                    context.commit(SET_AUTH, token);
                    resolve(response);
                })
                .catch(({ response }) => {
                    context.commit(SET_ERROR, response);
                    reject(response);
                });
        });
    },
    [CHECK_AUTH](context) {
        if (PassportService.getToken()) {
            ApiService.setHeader();
            ApiService.get("user")
                .then(({ data }) => {
                    context.commit(SET_AUTH, data.user);
                })
                .catch(({ response }) => {
                    context.commit(SET_ERROR, response.data.errors);
                });
        } else {
            context.commit(PURGE_AUTH);
        }
    },
    [UPDATE_USER](context, payload) {
        const { email, username, password, image, bio } = payload;
        const user = {
            email,
            username,
            bio,
            image
        };
        if (password) {
            user.password = password;
        }

        return ApiService.put("user", user).then(({ data }) => {
            context.commit(SET_AUTH, data.user);
            return data;
        });
    }
};

const mutations = {
    [SET_ERROR](state, error) {
        state.errors = error;
    },
    [SET_AUTH](state, user) {
        state.isAuthenticated = true;
        state.user = user;
        state.errors = {};
        PassportService.saveToken(state.user.token);
    },
    [PURGE_AUTH](state) {
        state.isAuthenticated = false;
        state.user = {};
        state.errors = {};
        PassportService.destroyToken();
    }
};

export default {
    state,
    actions,
    mutations,
    getters
};
